<%-- 
    Document   : calc
    Created on : 2 Dec 2020, 14:21:37
    Author     : simon
--%>

<%@page import="com.simonwoodworth.javamavencalcinclassdemo.Calculator"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello IS3313!</h1>
        <p>This is to demonstrate changes after the first commit and push.</p>
        <p><%= new java.util.Date() %></p>
        <p><%= new Calculator().add(7, 5) %></p>
        <p><%= new Calculator().subtract(7, 5) %></p>
        <p><%= new Calculator().multiply(7, 5) %></p>
        <p><%= new Calculator().divide(7, 5) %></p>
        <p><%= new Calculator().multiply(60, 10) %></p>
        <h2>Demonstrating feed through from NetBeans to finished app</h2>
        <p><%= new Calculator().add(100, 306) %></p>
        <p><%= new Calculator().percentage(7, 15) %></p>
    </body>
</html>
